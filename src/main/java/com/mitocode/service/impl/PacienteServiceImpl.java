package com.mitocode.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import com.mitocode.dao.IPacienteDAO;
import com.mitocode.model.Paciente;
import com.mitocode.service.IPacienteService;

@Named
public class PacienteServiceImpl implements IPacienteService, Serializable {

	@EJB
	private IPacienteDAO dao;

	@Override
	public Integer registrar(Paciente t) throws Exception {
		return dao.registrar(t);
	}

	@Override
	public Integer modificar(Paciente t) throws Exception {
		return dao.modificar(t);
	}

	@Override
	public List<Paciente> listar() throws Exception {
		return dao.listar();
	}

	@Override
	public Paciente listarPorId(Paciente t) throws Exception {
		return dao.listarPorId(t);
	}

}
