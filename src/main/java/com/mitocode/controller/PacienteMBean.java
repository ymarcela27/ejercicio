package com.mitocode.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mitocode.model.Paciente;
import com.mitocode.service.IPacienteService;

@Named
@ViewScoped
public class PacienteMBean implements Serializable {

	@Inject
	private IPacienteService service;
	
	private List<Paciente> lista;
	private Paciente paciente;

	@PostConstruct
	public void init() {
		this.paciente = new Paciente();
		this.lista = new ArrayList<>();
		this.listar();
	}

	public void listar() {
		try {
			setLista(service.listar());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private List<Paciente> getLista() {
		return lista;
	}

	private void setLista(List<Paciente> lista) {
		this.lista = lista;
	}

}
